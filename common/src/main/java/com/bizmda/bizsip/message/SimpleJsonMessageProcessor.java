package com.bizmda.bizsip.message;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;

/**
 * @author 史正烨
 */
public class SimpleJsonMessageProcessor extends AbstractMessageProcessor {
    @Override
    protected JSONObject biz2json(JSONObject inMessage) throws BizException {
        return inMessage;
    }

    @Override
    protected byte[] json2adaptor(JSONObject inMessage) throws BizException {
        return BizUtils.getBytes(JSONUtil.toJsonStr(inMessage));
    }

    @Override
    protected JSONObject adaptor2json(byte[] inBytes) throws BizException {
        return JSONUtil.parseObj(BizUtils.getString(inBytes));
    }

    @Override
    protected JSONObject json2biz(JSONObject inMessage) throws BizException {
        return inMessage;
    }
}
